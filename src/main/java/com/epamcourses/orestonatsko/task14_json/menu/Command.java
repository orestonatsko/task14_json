package com.epamcourses.orestonatsko.task14_json.menu;

public interface Command {
    void execute();
}
