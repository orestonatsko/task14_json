package com.epamcourses.orestonatsko.task14_json.parsers.jackson;

import com.epamcourses.orestonatsko.task14_json.model.Gem;
import com.epamcourses.orestonatsko.task14_json.model.GemParameters;
import com.epamcourses.orestonatsko.task14_json.parsers.JSONParser;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epamcourses.orestonatsko.task14_json.model.Gem.*;
import static com.epamcourses.orestonatsko.task14_json.model.GemParameters.*;

public class JACKSONGemParser extends JSONParser<Gem> {
    private List<Gem> gemList;

    @Override
    public void parse(File json) {
        try {
            getGemsWithJsonParser(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        setElements(gemList);
    }

    private void getGemsWithJsonParser(File json) throws IOException {
        gemList = new ArrayList<>();
        JsonParser parser = new JsonFactory().createParser(json);
        Gem gem = new Gem();
        JsonToken token;
        while (!parser.isClosed()) {
            token = parser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String field = parser.getCurrentName();
                parser.nextToken();
                switch (field) {
                    case NAME:
                        gem.setName(parser.getValueAsString());
                        break;
                    case ORIGIN:
                        gem.setOrigin(parser.getValueAsString());
                        break;
                    case VALUE:
                        gem.setValue(parser.getIntValue());
                        break;
                    case PRECIOUSNESS:
//                        gem.setPreciousness(Preciousness.PRECIOUS);
                        break;
                    case PARAMETERS:
                        gem.setParameters(getParameters(parser));
                        break;
                }
            } else if (JsonToken.END_OBJECT.equals(token)) {
                gemList.add(gem);
            }
        }

    }

    private GemParameters getParameters(JsonParser parser) throws IOException {
        JsonToken token = parser.nextToken();
        GemParameters parameters = new GemParameters();
        while (!JsonToken.END_OBJECT.equals(token)) {
            if (JsonToken.FIELD_NAME.equals(token)) {
                String name = parser.getCurrentName();
                parser.nextToken();
                switch (name) {
                    case COLOR:
                        parameters.setColor(parser.getValueAsString());
                        break;
                    case TRANSPARENCY:
                        parameters.setTransparency(parser.getIntValue());
                        break;
                    case FACES:
                        parameters.setFaces(parser.getIntValue());
                        break;
                }

            }
            token = parser.nextToken();
        }
        return parameters;
    }
}
