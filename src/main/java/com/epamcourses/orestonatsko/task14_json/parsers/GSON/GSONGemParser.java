package com.epamcourses.orestonatsko.task14_json.parsers.GSON;

import com.epamcourses.orestonatsko.task14_json.model.Gem;
import com.epamcourses.orestonatsko.task14_json.model.GemParameters;
import com.epamcourses.orestonatsko.task14_json.parsers.JSONParser;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epamcourses.orestonatsko.task14_json.model.Gem.*;
import static com.epamcourses.orestonatsko.task14_json.model.GemParameters.*;

public class GSONGemParser extends JSONParser<Gem> {
    private List<Gem> gemList;

    @Override
    public void parse(File json) {
        gemList = new ArrayList<>();
        try {
            JsonReader reader = new JsonReader(new FileReader(json));
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginObject();
                Gem gem = new Gem();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    switch (name) {
                        case NAME:
                            gem.setName(reader.nextString());
                            break;
                        case ORIGIN:
                            gem.setOrigin(reader.nextString());
                            break;
                        case VALUE:
                            gem.setValue(reader.nextInt());
                            break;
                        case PARAMETERS:
                            gem.setParameters(getGemParameters(reader));
                            break;
                        case PRECIOUSNESS:
                            gem.setPreciousness(Gem.Preciousness.valueOf(reader.nextString().toUpperCase()));
                            break;
                    }
                }
                reader.endObject();
                gemList.add(gem);
            }
            reader.endArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setElements(gemList);

    }

    private GemParameters getGemParameters(JsonReader reader) throws IOException {
        GemParameters parameters = new GemParameters();
        reader.beginObject();
        while (reader.hasNext()) {
            String nParameter = reader.nextName();
            switch (nParameter) {
                case COLOR:
                    parameters.setColor(reader.nextString());
                    break;
                case TRANSPARENCY:
                    parameters.setTransparency(reader.nextInt());
                    break;
                case FACES:
                    parameters.setFaces(reader.nextInt());
                    break;
            }
        }
        reader.endObject();
        return parameters;
    }
}
