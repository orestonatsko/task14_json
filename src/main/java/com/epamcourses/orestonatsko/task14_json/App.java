package com.epamcourses.orestonatsko.task14_json;

import com.epamcourses.orestonatsko.task14_json.menu.Menu;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

public class App {
    public static void main(String[] args) {
        URL jsonUrl = App.class.getClassLoader().getResource("json/gems.json");
        File json;
        try {
            if (jsonUrl != null) {
                json = new File(jsonUrl.toURI());
                new Menu(json).show();
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
