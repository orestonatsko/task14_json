package com.epamcourses.orestonatsko.task14_json.comparator;

import com.epamcourses.orestonatsko.task14_json.model.Gem;

import java.util.Comparator;

public class GemComparator implements Comparator<Gem> {
    @Override
    public int compare(Gem o1, Gem o2) {
        return o1.compareTo(o2);
    }
}
